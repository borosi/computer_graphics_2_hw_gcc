//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2014-tol.          
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk. 
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat. 
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni (printf is fajlmuvelet!)
// - new operatort hivni az onInitialization függvényt kivéve, a lefoglalt adat korrekt felszabadítása nélkül 
// - felesleges programsorokat a beadott programban hagyni
// - tovabbi kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan gl/glu/glut fuggvenyek hasznalhatok, amelyek
// 1. Az oran a feladatkiadasig elhangzottak ES (logikai AND muvelet)
// 2. Az alabbi listaban szerepelnek:  
// Rendering pass: glBegin, glVertex[2|3]f, glColor3f, glNormal3f, glTexCoord2f, glEnd, glDrawPixels
// Transzformaciok: glViewport, glMatrixMode, glLoadIdentity, glMultMatrixf, gluOrtho2D, 
// glTranslatef, glRotatef, glScalef, gluLookAt, gluPerspective, glPushMatrix, glPopMatrix,
// Illuminacio: glMaterialfv, glMaterialfv, glMaterialf, glLightfv
// Texturazas: glGenTextures, glBindTexture, glTexParameteri, glTexImage2D, glTexEnvi, 
// Pipeline vezerles: glShadeModel, glEnable/Disable a kovetkezokre:
// GL_LIGHTING, GL_NORMALIZE, GL_DEPTH_TEST, GL_CULL_FACE, GL_TEXTURE_2D, GL_BLEND, GL_LIGHT[0..7]
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : BOROS ISTVAN
// Neptun : D7M4N4
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy 
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem. 
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a 
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb 
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem, 
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.  
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat 
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#define _USE_MATH_DEFINES

#include <math.h>
#include <stdlib.h>

#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>                                                                                                                                                                                                           
#include <GLUT/glut.h>                                                                                                                                                                                                            
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)

#include <windows.h>

#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Innentol modosithatod...
const int screenWidth = 600;    // alkalmazás ablak felbontása
const int screenHeight = 600;
long baseTime = 30000;

//--------------------------------------------------------
// 3D Vektor
//--------------------------------------------------------
struct Vector {
    float x, y, z;

    Vector() {
        x = y = z = 0;
    }

    Vector(float x0, float y0, float z0 = 0) {
        x = x0;
        y = y0;
        z = z0;
    }

    Vector operator*(float a) {
        return Vector(x * a, y * a, z * a);
    }

    Vector operator-(int a) {
        return Vector(x - a, y - a, z - a);
    }

    Vector operator+(int a) {
        return Vector(x + a, y + a, z + a);
    }

    Vector operator/(float a) {
        return Vector(x / a, y / a, z / a);
    }

    Vector operator+(const Vector &v) {
        return Vector(x + v.x, y + v.y, z + v.z);
    }

    Vector operator-(const Vector &v) {
        return Vector(x - v.x, y - v.y, z - v.z);
    }

    Vector operator/(const Vector &v) {
        return Vector(x / v.x, y / v.y, z / v.z);
    }

    Vector operator-() const {
        return Vector(-x, -y, -z);
    }

    float operator*(const Vector &v) {   // dot product
        return (x * v.x + y * v.y + z * v.z);
    }

    Vector operator%(const Vector &v) {  // cross product
        return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
    }

    float Length() { return sqrt(x * x + y * y + z * z); }

    Vector normalize() {
        float l = Length();
        return Vector(x / l, y / l, z / l);
    }

    Vector mul(const Vector v) {
        return Vector(x * v.x, y * v.y, z * v.y);
    }

    Vector rotateAroundAxis(Vector w, float phi) {
        phi = (phi * M_PI) / 180;
        return (*this) * cos(phi) + w * ((*this) * w) * (1 - cos(phi)) + w % (*this) * sin(phi);
    }
};

struct Color {
    float r, g, b;

    Color() {
        r = g = b = 0;
    }

    Color(float r0, float g0, float b0) {
        r = r0;
        g = g0;
        b = b0;
    }

    Color operator*(float a) {
        return Color(r * a, g * a, b * a);
    }

    Color operator/(float a) {
        return Color(r / a, g / a, b / a);
    }

    Color operator*(const Color &c) {
        return Color(r * c.r, g * c.g, b * c.b);
    }

    Color operator/(const Color &c) {
        return Color(r / c.r, g / c.g, b / c.b);
    }

    Color operator+(const Color &c) {
        return Color(r + c.r, g + c.g, b + c.b);
    }

    Color operator-(const Color &c) {
        return Color(r - c.r, g - c.g, b - c.b);
    }

    Color operator+(float a) {
        return Color(r + a, g + a, b + a);
    }

    Color operator-(float a) {
        return Color(r - a, g - a, b - a);
    }

    Color sqrt() {
        return Color(sqrtf(r), sqrtf(g), sqrtf(b));
    }
};

Color image[screenWidth * screenHeight];    // egy alkalmazás ablaknyi kép
long lastRender = 0;
float EPSILON = 1e-3;

class Texture {
public:
    Texture() { }

    virtual Color getColor(Vector pos, Vector normal) = 0;
};

class PlainTexture : public Texture {
    Color kd;
public:
    PlainTexture(Color kd) : kd(kd) { }

    Color getColor(Vector pos, Vector normal) { return kd; }
};

class RectTexture : public Texture {
    Color kd;
public:
    RectTexture(Color color) : kd(color) { }

    Color getColor(Vector pos, Vector normal) {
        Vector w = normal % Vector(0, 0, -1);
        float phi = normal * Vector(0, 0, -1);
        phi = acosf(phi);
        Vector hitpos = pos * cosf(phi) +
                        w % pos * sinf(phi) +
                        w * (pos * w) * (1 - cos(phi));

        if (fmod(fabs(hitpos.x), 50) < 25 && fmod(fabs(hitpos.y), 50) > 25)
            return kd * 0.4;
        return kd;
    }
};

class Material {
protected:
    bool reflective;
    bool refractive;
public:
    bool isReflective() { return reflective; }

    bool isRefractive() { return refractive; }

    virtual Vector reflect(Vector inDir, Vector normal) { return Vector(0, 0, 0); }

    virtual Vector refract(Vector inDir, Vector normal) { return Vector(0, 0, 0); }

    virtual Color Fresnel(Vector inDir, Vector normal) { return Color(0, 0, 0); }

    virtual Color computeKd(Vector pos, Vector normal) { return Color(0, 0, 0); }

    virtual Color shade(Vector normal, Vector viewDir, Vector lightDir, Color inRad) { return Color(0, 0, 0); }
};

class RoughMaterial : public Material {
    Color kd, ks;
    float shininess;
    Texture *texture;
public:
    RoughMaterial(Texture *texture, Color kd, Color ks, float shininess) : texture(texture), kd(kd), ks(ks),
                                                                           shininess(shininess) {
        reflective = false;
        refractive = false;
    }

    Color shade(Vector normal, Vector viewDir, Vector lightDir, Color inRad) {
        Color reflRad(0, 0, 0);
        float cosTheta = normal * lightDir;
        if (cosTheta < 0) return reflRad;
        reflRad = inRad * kd * cosTheta;
        Vector halfWay = (viewDir + lightDir).normalize();  //*-1
        float cosDelta = normal * halfWay;
        if (cosDelta < 0) return reflRad;
        return reflRad + inRad * ks * pow(cosDelta, shininess);
    }

    Color computeKd(Vector pos, Vector normal) {
        kd = texture->getColor(pos, normal);
    }

    ~RoughMaterial() {
        delete texture;
    }
};

class SmoothMaterial : public Material {
    Color F0;
    Color N;
public:
    SmoothMaterial(Color N, Color k, bool isReflective, bool isRefractive) {
        this->N = N;
        reflective = isReflective;
        refractive = isRefractive;
        Color a = (N - 1) * (N - 1);
        Color b = (N + 1) * (N + 1);
        F0.r = ((a + k.r * k.r) / (b + k.r * k.r)).r;
        F0.g = ((a + k.g * k.g) / (b + k.g * k.g)).g;
        F0.b = ((a + k.b * k.b) / (b + k.b * k.b)).b;
    }

    Vector reflect(Vector inDir, Vector normal) {
        return inDir - normal * (normal * inDir) * 2.0f;
    }

    Vector refract(Vector inDir, Vector normal) {
        Color ior = N;
        float a = normal * inDir * -1.0f;
        Color cosa = Color(a, a, a);
        if (cosa.r < 0 || cosa.g < 0 || cosa.b < 0) {
            cosa = cosa * (-1.0f);
            normal = normal * -1.0f;
            ior = Color(1, 1, 1) / N;
        }
        Color disc = Color(1, 1, 1) - (Color(1, 1, 1) - (cosa * cosa)) / ior / ior;
        if (disc.r < 0 || disc.g < 0 || disc.b < 0)
            return reflect(inDir, normal);
        Color inDirHelp = Color(inDir.x, inDir.y, inDir.z);
        Color normalHelp = Color(normal.x, normal.y, normal.z);
        Color c = inDirHelp / ior + normalHelp * (cosa / ior - disc.sqrt());
        return Vector(c.r, c.g, c.b);
    }

    Color Fresnel(Vector inDir, Vector normal) {
        float cosa = fabs(normal * inDir);
        return F0 + (Color(1, 1, 1) - F0) * pow(1 - cosa, 5);
    }

};

struct Hit {
    float t;
    Vector position;
    Vector normal;
    Material *material;
    Vector velocity;

    Hit() {
        t = -1;
        velocity = Vector(0, 0, 0);
    }
};

struct Ray {
    Vector origin;
    Vector direction;
    float speed;

    Ray() { }

    Ray(Vector o, Vector d) : origin(o), direction(d), speed(100) { }

    Vector getVelocity() {
        return direction * speed;
    }
};

class Intersectable {
protected:
    Material *material;
public:
    bool isMoving;

    Intersectable() { isMoving = false; }

    virtual Hit intersect(Ray ray) = 0;

    virtual void move(long elapsedTime) { };

    virtual void setPosition(float t) { }

    ~Intersectable() {
        delete material;
    }
};

struct Light {
    Vector position;
    Vector rayPos;
    Color intensity;
    Vector speed;
    bool isMoving;

    Light(Vector p, Color i, Vector speed) : position(p), rayPos(p), intensity(i), speed(speed * 100),
                                             isMoving(true) { }

    Vector getPastPosition(Ray ray) {
        Vector s = position - ray.origin;
        float t = s.Length() / ray.getVelocity().Length();
        rayPos = position - (speed * t);
        return rayPos;
    }

    Vector getLightDirection(Vector position) {
        Vector d = (this->rayPos - position - speed).normalize();
        return d;
    }

    void setPosition(float t) {
        position = position + speed * t;
    }

    float Attenuation(Hit hit) {
        return pow(1.0f / (((rayPos - hit.position)).Length() / 100), 2);
    }

    float Intensity(Hit hit) {
        return maximum(hit.normal * (rayPos - hit.position).normalize(), 0.0f);
    }

    float maximum(float a, float b) {
        return a > b ? a : b;
    }

    Color getIntesi(Hit hit) {
        return intensity * Intensity(hit) * Attenuation(hit);
    }

    void move(long elapsedTime) {
        position = position + (speed / 1000) * elapsedTime;
    }
};

class Camera {
    Vector eye;
    Vector lookat;
    Vector up;
    Vector right;
public:
    Camera() { }

    Camera(Vector eye, Vector lookat) : eye(eye), lookat(lookat) {
        up = Vector(0, 1, 0);
        right = Vector(1, 0, 0);
    }

    Vector getViewDirection(Vector position) { return (eye - position).normalize(); }

    Ray getRay(int x, int y) {
        Vector o = Vector((float) (x - lookat.x), (float) (y - lookat.y), lookat.z);
        Vector d = (o - eye).normalize();
        return Ray(o, d);
    }
};

class Scene {
    Camera camera;
    Intersectable *objects[100];
    Light *lights[100];
    int numOfObjects;
    int numOfLights;
    int maxDepth;
public:
    Scene() {
        numOfObjects = 0;
        numOfLights = 0;
        maxDepth = 10;
        camera = Camera(Vector(0, 0, -600), Vector(300, 300, -300));
    }

    void addObject(Intersectable *obj) {
        objects[numOfObjects++] = obj;
    }

    void addLight(Light *light) {
        lights[numOfLights++] = light;
    }

    void render() {
        Color La = Color(0, 0, 0);
        for (int y = 0; y < screenHeight; y++) {
            for (int x = 0; x < screenWidth; x++) {
                Ray ray = camera.getRay(x, y);
                Color color = trace(ray, 0, 0);
                image[y * screenWidth + x] = color;
            }
        }
    }

    Hit firstIntersect(Ray ray) {
        Hit bestHit;
        for (int i = 0; i < numOfObjects; ++i) {
            Hit hit = objects[i]->intersect(ray);
            if (hit.t > 0 && (bestHit.t < 0 || hit.t < bestHit.t))
                bestHit = hit;
        }
        return bestHit;
    }

    Color trace(Ray ray, int depth, float rayTime) {
        if (depth > maxDepth) {
            return Color(0, 0, 0);
        }
        Hit hit = firstIntersect(ray);
        if (hit.t < 0)
            return Color(0, 0, 0);
        Color outRadiance(0, 0, 0);

        rayTime += hit.t;
        goBackInTime(-hit.t);
        hit.position = hit.position - hit.velocity * hit.t;

        for (int i = 0; i < numOfLights; i++) {
            Vector N = hit.normal.normalize();
            Vector Li = lights[i]->getLightDirection(hit.position);
            Ray shadowRay(hit.position + N * EPSILON, Li);
            Vector V = camera.getViewDirection(hit.position);
            Color c = lights[i]->getIntesi(hit);
            Hit shadowHit = firstIntersect(shadowRay);
            if (shadowHit.t < 0 || (shadowHit.t > ((hit.position - lights[i]->getPastPosition(shadowRay))).Length() /
                                                  ray.getVelocity().Length())) {
                if ((shadowHit.t + rayTime) < ((float) lastRender / 1000)) {
                    hit.material->computeKd(hit.position, hit.normal);
                    outRadiance = outRadiance + hit.material->shade(N, V, Li, c);
                }
            }
        }
        if (hit.material->isReflective()) {
            Vector reflectionDir = hit.material->reflect(ray.direction, hit.normal.normalize());
            Ray reflectedRay(hit.position + hit.normal * EPSILON, reflectionDir);
            outRadiance = outRadiance +
                          trace(reflectedRay, depth + 1, rayTime) * hit.material->Fresnel(ray.direction, hit.normal);
        }
        if (hit.material->isRefractive()) {
            Vector refractionDir = hit.material->refract(ray.direction, hit.normal.normalize());
            Ray refractedRay = Ray(hit.position - hit.normal * EPSILON, refractionDir);
            outRadiance = outRadiance + trace(refractedRay, depth + 1, rayTime) *
                                        (Color(1, 1, 1) - hit.material->Fresnel(ray.direction, hit.normal));
        }

        goBackInTime(hit.t);

        return outRadiance;
    }

    void animate() {
        long elapsedTime = glutGet(GLUT_ELAPSED_TIME) - lastRender + baseTime;
        for (int i = 0; i < numOfObjects; i++)
            if (objects[i]->isMoving)
                objects[i]->move(elapsedTime);
        for (int i = 0; i < numOfLights; i++) {
            if (lights[i]->isMoving) {
                lights[i]->move(elapsedTime);
            }
        }
    }

    void goBackInTime(float t) {
        for (int i = 0; i < numOfObjects; i++)
            objects[i]->setPosition(t);
        for (int i = 0; i < numOfLights; i++)
            lights[i]->setPosition(t);
    }

    ~Scene() {
        delete objects;
        delete lights;
    }

};

class Sphere : public Intersectable {
public:
    Vector center;
    float radius;
    Vector speed;

    Sphere(Vector c, float r, Material *material) {
        center = c;
        radius = r;
        speed = Vector(0, 0, 0);
        this->material = material;
        this->isMoving = true;
    }

    Hit intersect(Ray ray) {
        Hit hit = Hit();
        float a, b, c;
        Vector vel, pos;
        vel = ray.getVelocity() + speed;
        pos = ray.origin;
        a = vel * vel;
        b = (pos - center) * vel * 2.0;
        c = (pos - center) * (pos - center) - radius * radius;

        float disk = b * b - 4 * a * c;
        if (disk < 0)
            return hit;

        float t1 = (-1.0 * b - sqrt(disk)) / (2.0 * a);
        float t2 = (-1.0 * b + sqrt(disk)) / (2.0 * a);

        float t = t1;
        if (t1 < 0)
            t = t2;
        if ((t > EPSILON)) {
            hit.t = t;
            hit.position = ray.origin + (this->speed + vel) * hit.t;
            Vector normal = hit.position - center;
            hit.normal = (hit.position - center).normalize();
            hit.material = this->material;
            hit.velocity = speed;
            return hit;
        }
    }

    void move(long elapsedTime) {
        center = center + (speed / 1000) * elapsedTime;
    }

    void setPosition(float t) {
        center = center + speed * t;
    }
};

class Plane : public Intersectable {
    Vector position;
    Vector normal;
    Vector speed;
public:
    Plane(Vector position, Vector normal, Material *material) : position(position), normal(normal.normalize()) {
        this->material = material;
    }

    Hit intersect(Ray ray) {
        Hit hit;
        float t = ((normal * position) - (normal * ray.origin)) / (normal * ray.getVelocity());
        if (t > EPSILON) {
            hit.t = t;
            hit.position = ray.origin + ray.getVelocity() * hit.t;
            hit.normal = normal;
            hit.material = this->material;
            hit.velocity = this->speed;
            return hit;
        }
        return hit;
    }
};

class Paraboloid : public Intersectable {
    Vector center;
    float b;
    bool konvex;
public:
    Paraboloid(Vector center, float b, bool konvex, Material *material) :
            center(center), b(b), konvex(konvex) { this->material = material; }

    Hit intersect(Ray ray) {
        Vector V = ray.getVelocity();
        Vector P = ray.origin;
        float dir;
        (konvex) ? dir = -1.0f : dir = 1.0f;
        float A = (V.x * V.x + V.y * V.y);
        float B = 2 * (V.x * (P.x - center.x)) + 2 * (V.y * (P.y - center.y)) - V.z * dir * b * b;
        float C = ((P.x - center.x) * (P.x - center.x) + (P.y - center.y) * (P.y - center.y)) -
                  (P.z - center.z) * dir * b * b;

        float disc = B * B - 4 * A * C;

        Hit hit;

        if (disc < 0)
            return hit;

        float t1 = (-B + sqrt(disc)) / (2 * A);
        float t2 = (-B - sqrt(disc)) / (2 * A);

        float t = t1;
        if (t1 < 0)
            t = t2;

        if (A == 0) {
            t = -C / B;
        }

        if ((t > EPSILON)) {
            hit.t = t;
            hit.position = P + V * hit.t;
            hit.material = material;
            Vector normal;
            (konvex) ? normal = Vector(-2 * hit.position.x, -2 * hit.position.y, -b * b) :
                    normal = Vector(2 * hit.position.x, 2 * hit.position.y, -b * b);
            hit.normal = normal.normalize();
            return hit;
        }
    }
};

class Ellipsoid : public Intersectable {
    Vector center;
    Vector speed;
    float a;
    float b;
    float c;
public:
    Ellipsoid(Vector center, float a, float b, float c, Material *material, Vector speed) :
            center(center), a(a), b(b), c(c), speed(speed * 100) {
        this->material = material;
        this->isMoving = true;
    }

    Hit intersect(Ray ray) {
        Vector axis = Vector(-1, 0, 1);
        float phi = 60;
        Vector origin = ray.origin - center;
        Vector tfOrigin = origin.rotateAroundAxis(axis, phi);
        Ray tfRay = ray;
        tfRay.origin = ray.origin + (tfOrigin - origin);
        tfRay.direction = (ray.getVelocity() + speed).rotateAroundAxis(axis, phi);

        Vector P = tfRay.origin;
        Vector V = tfRay.direction;

        Vector denom = Vector(1 / a, 1 / b, 1 / c);
        float A = (V.mul(denom) * V.mul(denom));
        float B = (P.mul(denom).mul(denom) - center.mul(denom).mul(denom)) * V * 2;
        float C = (P.mul(denom) - center.mul(denom)) * (P.mul(denom) - center.mul(denom)) - 1;

        float disc = B * B - 4 * A * C;

        Hit hit;

        if (disc < 0)
            return hit;

        float t1 = (-B - sqrt(disc)) / (2 * A);
        float t2 = (-B + sqrt(disc)) / (2 * A);

        float t = t1;
        if (t1 < 0)
            t = t2;

        if (A == 0)
            t = -C / B;

        if ((t > EPSILON)) {
            hit.t = t;
            hit.position = P + V * hit.t;
            hit.material = material;

            Vector dist = (hit.position - center);
            Vector denominator = Vector(1 / (a * a), 1 / (b * b), 1 / (c * c));
            Vector normal = (dist * 2).mul(denominator);

            normal = normal.rotateAroundAxis(axis, -phi);
            normal = normal.normalize();

            hit.normal = normal;

            Vector d = (hit.position - center).rotateAroundAxis(axis, -phi);
            Vector to = d - (hit.position - center);
            hit.position = hit.position + to;
            return hit;
        }
    }

    void move(long elapsedTime) {
        center = center + (speed / 1000) * elapsedTime;
    }
};

Scene s;

// Inicializacio, a program futasanak kezdeten, az OpenGL kontextus letrehozasa utan hivodik meg (ld. main() fv.)
void onInitialization() {
    glViewport(0, 0, screenWidth, screenHeight);

    s = Scene();
    s.addObject(new Plane(Vector(500, 0, 0), Vector(-1, 0, 0),
                          new RoughMaterial(new RectTexture(Color(1, 1, 0)), Color(1, 1, 0), Color(1, 1, 1),
                                            200)));  //right
    s.addObject(new Plane(Vector(-500, 0, 0), Vector(1, 0, 0),
                          new RoughMaterial(new RectTexture(Color(1, 0, 0)), Color(1, 0, 0), Color(1, 1, 1),
                                            200)));  //left
    s.addObject(new Plane(Vector(0, 500, 0), Vector(0, -1, 0),
                          new RoughMaterial(new RectTexture(Color(0, 1, 1)), Color(0, 1, 1), Color(1, 1, 1),
                                            200)));  //top
    s.addObject(new Plane(Vector(0, -500, 0), Vector(0, 1, 0),
                          new RoughMaterial(new RectTexture(Color(0, 1, 0)), Color(0, 1, 0), Color(1, 1, 1),
                                            200)));  //bottom
//    s.addObject(new Plane(Vector(0, 0, 500), Vector(0, 0, -1), new RoughMaterial(new RectTexture(Color(0, 0, 1)),Color(0, 0, 1), Color(1,1,1), 200)));  //front
    s.addObject(new Paraboloid(Vector(0, 0, (500 + 408.16f)), 35, true,
                               new SmoothMaterial(Color(0.17, 0.35, 1.5), Color(3.1, 2.7, 1.9), true, false)));
    s.addObject(new Plane(Vector(0, 0, -500), Vector(0, 0, 1),
                          new RoughMaterial(new RectTexture(Color(1, 1, 1)), Color(1, 0, 1), Color(1, 1, 1),
                                            200)));  //back

    Light *light = new Light(Vector(-100, 0, -350), Color(100, 100, 100), Vector(0, 0, 0.2));
    light->move((float) baseTime);
    s.addLight(light);

    s.addObject(new Ellipsoid(Vector(0, -150, 200), 200, 100, 50,
                              new SmoothMaterial(Color(1.5, 1.5, 1.5), Color(0, 0, 0), true, true),
                              Vector(0.3826, 0.25, -0.25)));
}

// Rajzolas, ha az alkalmazas ablak ervenytelenne valik, akkor ez a fuggveny hivodik meg
void onDisplay() {
    glClearColor(0.1f, 0.2f, 0.3f, 1.0f);        // torlesi szin beallitasa
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // kepernyo torles

    // ..

    // Peldakent atmasoljuk a kepet a rasztertarba
    glDrawPixels(screenWidth, screenHeight, GL_RGB, GL_FLOAT, image);

    glutSwapBuffers();                    // Buffercsere: rajzolas vege

}

// Billentyuzet esemenyeket lekezelo fuggveny (lenyomas)
void onKeyboard(unsigned char key, int x, int y) {
    if (key == 32) {
        s.render();
        glutPostRedisplay();        // d beture rajzold ujra a kepet
    }
}

// Billentyuzet esemenyeket lekezelo fuggveny (felengedes)
void onKeyboardUp(unsigned char key, int x, int y) {

}

// Eger esemenyeket lekezelo fuggveny
void onMouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON &&
        state == GLUT_DOWN)   // A GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON illetve GLUT_DOWN / GLUT_UP
        glutPostRedisplay();                         // Ilyenkor rajzold ujra a kepet
}

// Eger mozgast lekezelo fuggveny
void onMouseMotion(int x, int y) {

}

// `Idle' esemenykezelo, jelzi, hogy az ido telik, az Idle esemenyek frekvenciajara csak a 0 a garantalt minimalis ertek
void onIdle() {
    lastRender = glutGet(GLUT_ELAPSED_TIME) + baseTime;
    s.animate();
}

// ...Idaig modosithatod
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// A C++ program belepesi pontja, a main fuggvenyt mar nem szabad bantani
int main(int argc, char **argv) {
    glutInit(&argc, argv);                // GLUT inicializalasa
    glutInitWindowSize(600, 600);            // Alkalmazas ablak kezdeti merete 600x600 pixel
    glutInitWindowPosition(100, 100);            // Az elozo alkalmazas ablakhoz kepest hol tunik fel
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);    // 8 bites R,G,B,A + dupla buffer + melyseg buffer

    glutCreateWindow("Grafika hazi feladat");        // Alkalmazas ablak megszuletik es megjelenik a kepernyon

    glMatrixMode(GL_MODELVIEW);                // A MODELVIEW transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);            // A PROJECTION transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();

    onInitialization();                    // Az altalad irt inicializalast lefuttatjuk

    glutDisplayFunc(onDisplay);                // Esemenykezelok regisztralasa
    glutMouseFunc(onMouse);
    glutIdleFunc(onIdle);
    glutKeyboardFunc(onKeyboard);
    glutKeyboardUpFunc(onKeyboardUp);
    glutMotionFunc(onMouseMotion);

    glutMainLoop();                    // Esemenykezelo hurok

    return 0;
}

